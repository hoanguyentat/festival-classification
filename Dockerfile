FROM pdoviet/tensorflow:latest

RUN mkdir -p app/im/tmp
ADD bin /app/im/bin
ADD retrained_graph.pb /app/im/
ADD retrained_labels.txt /app/im/
ADD server.py /app/im/
# set working dire
WORKDIR  app/im

# Expose the application on port 8001
EXPOSE 8002
CMD ["bin/start.sh"]

