#!/usr/bin/env bash
VERSION=$(date +"%y.%m.%d")
WORKING_DIR=$(pwd)
DOCKER_IMAGE=images-classification
echo 'Login to Docker hub'
docker login ir.admicro.vn
echo 'Build docker image'
docker build -t ir.admicro.vn/phuongdv/$DOCKER_IMAGE:$VERSION .
echo 'Push to Docker hub'
docker push  ir.admicro.vn/phuongdv/$DOCKER_IMAGE:$VERSION
