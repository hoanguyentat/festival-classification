import MySQLdb as my
from MySQLdb.cursors import DictCursor
from six.moves.urllib.request import urlretrieve
import os
import sys
import tensorflow as tf
import redis
import time
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage

reload(sys)
sys.setdefaultencoding('utf-8')

#table to get and insert data
images_table = "urlimages"
articles_table = "urlarticles"

festival_dir = "data1/festival"
nonfestival_dir = "data1/nonfestival"
redis_dir = "./redis"

filename = "account.txt"
redisfile = "redis.txt"
label_lines = [line.rstrip() for line in tf.gfile.GFile("retrained_labels.txt")]

# Ket noi toi db
def get_account():
	account = []
	f = open(filename, "r")
	for line in f:
		account.append(line.rstrip())
	f.close()
	return account

def connect_redis():
	redis_arr = []
	f = open(redisfile, "r")
	for line in f:
		redis_arr.append(line.rstrip())
	f.close()
	r = redis.Redis(host=redis_arr[0], port=redis_arr[1], password=redis[2])
	return r

def creat_graph():
	with tf.gfile.FastGFile("retrained_graph.pb", 'rb') as f:
		graph_def = tf.GraphDef()
		graph_def.ParseFromString(f.read())
		tf.import_graph_def(graph_def, name='')

def predict_images(image):
	result = {}
	if not tf.gfile.Exists(image):
		tf.logging.fatal('File does not exist %s', image)
	image_data = tf.gfile.FastGFile(image, 'rb').read()

	with tf.Session() as sess:
		# Feed the image_data as input to the graph and get first prediction
		softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
		
		predictions = sess.run(softmax_tensor, \
				 {'DecodeJpeg/contents:0': image_data})
		
		# Sort to show labels of first prediction in order of confidence
		top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
		
		for node_id in top_k:
			human_string = label_lines[node_id]
			score = predictions[0][node_id]
			print('%s (score = %.5f)' % (human_string, score))
		result[0] = str(predictions[0][1])
		result[1] = str(predictions[0][0])
	return result

def get_data():
	account = get_account()
	db = my.connect(host=account[0], user=account[1], passwd=account[2], db=account[3], charset='utf8', cursorclass=DictCursor)
	cursor = db.cursor()
	# data = cursor.execute("select * from " + images_table + " where IsRead = 0")
	data = cursor.execute("select * from " + images_table)
	update_table = "update urlimages set IsRead=1 where Url=%s"
	result = cursor.fetchall()

	last_update = ""
	count_update = 0
	# redis connection
	redis_cursor = connect_redis()
	for row in result:
		url_image = str(row['Url'])
		images_name_from_url = url_image.split('/')[-1]
		dest_filename = os.path.join(redis_dir, images_name_from_url)
		try:
			print("Downloaded image in url %s" % row['Url'])
			images_name, _ = urlretrieve(url_image, dest_filename)
			val = predict_images(dest_filename)

			# Cap nhat url da duoc du doan, day du lieu vao redis
			if redis_cursor.hset(url_image, val):
				count_update+=1
				cursor.execute(update_table, (url_image))
			os.remove(dest_filename)
		except Exception as e:
			print("File not download...")
			pass	
	cursor.close()
	db.close()

if __name__ == '__main__':
	creat_graph()
	get_data()

	