import MySQLdb as my
from MySQLdb.cursors import DictCursor
from six.moves.urllib.request import urlretrieve
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


#table to get and insert data
images_table = "urlimages"
articles_table = "urlarticles"

festival_dir = "data1/festival"
nonfestival_dir = "data1/nonfestival"

# Ket noi toi db
filename = "account.txt"
account = []
f = open(filename, "r")
for line in f:
	account.append(line.rstrip())
f.close()

def get_data():
	db = my.connect(host=account[0], user=account[1], passwd=account[2], db=account[3], charset='utf8', cursorclass=DictCursor)
	cursor = db.cursor()
	data = cursor.execute("select * from " + images_table + " where ArticleId = 21406")
	# data = cursor.execute("select * from " + images_table)
	result = cursor.fetchall()

	count_festival = 1
	count_nonfestival = 1
	for x in result:
		images_name_from_url = str(x['Url']).split('/')[-1]
		if x['ArticleId'] == 21406:
			dest_filename = os.path.join(festival_dir, "festival" + str(count_festival) + ".jpg")
			count_festival+=1
		else:
			dest_filename = os.path.join(nonfestival_dir, "nonfestival" + str(count_nonfestival) + ".jpg")
			count_nonfestival += 1
		try:
			print("Downloaded image in url %s" % x['Url'])
			images_name, _ = urlretrieve(str(x['Url']), dest_filename)
		except Exception as e:
			print("File not download...")
			pass	
	cursor.close()
	db.close()

	