from flask import Flask
from flask import request
from flask import jsonify
from flask import Response

import json
import tensorflow as tf
from six.moves.urllib.request import urlretrieve
from flask_cors import CORS
import time
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage

import os, sys

app = Flask(__name__)
cors = CORS(app)
# Loads label file, strips off carriage return
label_lines = [line.rstrip() for line in tf.gfile.GFile("retrained_labels.txt")]
data_dir = './tmp/'

def creat_graph():
	with tf.gfile.FastGFile("retrained_graph.pb", 'rb') as f:
		graph_def = tf.GraphDef()
		graph_def.ParseFromString(f.read())
		tf.import_graph_def(graph_def, name='')

def predict_images(image):
	result = {}
	if not tf.gfile.Exists(image):
		tf.logging.fatal('File does not exist %s', image)
	image_data = tf.gfile.FastGFile(image, 'rb').read()

	with tf.Session() as sess:
		# Feed the image_data as input to the graph and get first prediction
		softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
		
		predictions = sess.run(softmax_tensor, \
				 {'DecodeJpeg/contents:0': image_data})
		
		# Sort to show labels of first prediction in order of confidence
		top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
		
		for node_id in top_k:
			human_string = label_lines[node_id]
			score = predictions[0][node_id]
			print('%s (score = %.5f)' % (human_string, score))
		result[0] = str(predictions[0][1])
		result[1] = str(predictions[0][0])
	# return float(predictions[0][0]) > float(predictions[0][1])
	return result


@app.route('/api/list', methods=['POST'])
def predict():
	print("get requets from client")
	dic = {}
	body = request.get_data().decode('utf8').replace("'", '"')

	# Get time for file name
	start = time.strftime("%H%M%S")
	if body == "":
		return jsonify(error="Images url not found")
	else:
		list_url = json.loads(body)
	check = "No"
	for url in list_url:
		url_image = str(url)
		file_name = start + "_+_" + url_image.split('/')[-1]
		dest_filename = os.path.join(data_dir, file_name)

		try:
			image_name, _ = urlretrieve(url_image, dest_filename)
			# im = plt.imread(dest_filename)
			im = ndimage.imread(dest_filename)
			if im.shape[0] < 300 or im.shape[1] < 300:
				print("Image size too small")
			else:
				dic[url_image] = predict_images(dest_filename)
				# check = "Yes"
				# os.remove(dest_filename)
				# break
			os.remove(dest_filename)
		except Exception as e:
			print(e)
	# print(dic)
	return jsonify(dic)

if __name__ == '__main__':
	creat_graph()
	app.run(host='0.0.0.0')
