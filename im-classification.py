from __future__ import print_function
import matplotlib.pyplot as plt 
import numpy as np
import os
import sys

from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle
import tensorflow as tf 
import cv2

data_root = './data/'
data_train_dir = './data/train/'
data_test_dir = './data/test/'

def get_data_folder(data_folders):
	data_dir = []
	folders = os.listdir(data_folders)
	print(folders)
	for folder in folders:
		if folder.split('.')[-1] != 'pickle':
		  data_dir.append(os.path.join(data_folders, folder))
	return data_dir

train_folder = get_data_folder(data_train_dir)
test_folder = get_data_folder(data_test_dir)

# print(train_folder)
# print(test_folder)

image_size = 512
pixel_depth = 255.0

def load_letter(folder, min_num_images):
	image_files = os.listdir(folder)
	dataset = np.ndarray(shape=(len(image_files), image_size, image_size), dtype=np.float32)
	# print(dataset.shape)
	num_images = 0
	for image in image_files:
		image_file = os.path.join(folder, image)
		try:
			image_data = (ndimage.imread(image_file).astype(float) - pixel_depth / 2) / pixel_depth
			# print(image_data.shape)
			image_data=image_data.resize((image_size, image_size))
			resized_image = np.ndarray(image_data, dtype=np.float32)
			dataset[num_images, :, :] = resized_image
			num_images = num_images + 1
		except IOError as e:
			print('Could not read: ' + image_file, ':', e, '- it\'s ok, skipping.')
	dataset = dataset[0:num_images, :, :]
	if num_images < min_num_images:
		raise Exception('Many fewer images then expected')
	print("Full dataset tensor: ", dataset.shape)
	print("Mean: ", np.mean(dataset))
	print("Standard deviation: ", np.std(dataset))
	return dataset

def maybe_pickle(data_folders, min_num_images_per_class, force=False):
	dataset_names = []
	for folder in data_folders:
		set_filename = folder + '.pickle'
		dataset_names.append(set_filename)
		print(os.path.exists(set_filename))
		if os.path.exists(set_filename) and not force:
			print("%s da ton tai." % set_filename)
		else:
			print("Bat dau nen file %s" % set_filename)
			dataset = load_letter(folder, min_num_images_per_class)
			try:
				with open(set_filename, 'wb') as f:
					pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
			except Exception as e:
				print(e)
	return dataset_names

train_datasets = maybe_pickle(train_folder, 1000)
test_datasets = maybe_pickle(test_folder,200)

def make_arrays(nb_rows, img_size):
	 if nb_rows:
		 dataset = np.ndarray((nb_rows, img_size, img_size), dtype=np.float32)
		 labels = np.ndarray(nb_rows, dtype=np.int32)
	 else:
		 dataset, labels = None, None
	 return dataset, labels


def merge_datasets(pickle_files, train_size, valid_size=0):
	 valid_dataset, valid_labels = make_arrays(valid_size * 10, image_size)  # 10k valid
	 train_dataset, train_labels = make_arrays(train_size, image_size)  # 200k train
	 vsize_per_class = valid_size
	 tsize_per_class = train_size

	 start_v, start_t = 0, 0
	 end_v, end_t = vsize_per_class, 0
	 end_l = vsize_per_class + tsize_per_class  # 210k
	 count = 0
	 length_train = 0
	 for label, pickle_file in enumerate(pickle_files):
		 try:
			 with open(pickle_file, 'rb') as f:
				 count += 1
				 print("Count file: %d" % count)
				 letter_set = pickle.load(f)
				 np.random.shuffle(letter_set)
				 if valid_dataset is not None:
					 valid_letter = letter_set[:vsize_per_class, :, :]
					 valid_dataset[start_v:end_v, :, :] = valid_letter
					 valid_labels[start_v:end_v] = label
					 start_v += vsize_per_class
					 end_v += vsize_per_class
					 print("index of valid: %d %d, index of train: %d %d" % (start_v, end_v, start_t, end_t))
				 len_train_letter = len(letter_set) - vsize_per_class
				 length_train += len_train_letter
				 train_letter = letter_set[vsize_per_class:len(letter_set), :, :]
				 print("Len train letter of file: %d" % len(train_letter))
				 end_t += len_train_letter
				 print("Len train dataset of file: %d" % len(train_dataset[start_t:end_t, :, :]))
				 print("vsize_per_class: %d, len a file: %d" % (vsize_per_class, len(letter_set)))
				 train_dataset[start_t:end_t, :, :] = train_letter
				 train_labels[start_t:end_t] = label
				 start_t += len_train_letter
				 print("----------------------------------------------")
		 except Exception as e:
			 print('Unable to process data from', pickle_file, ':', e)
			 raise
	 print('Len train dataset: %d' % length_train)
	 return valid_dataset, valid_labels, train_dataset, train_labels


train_size = 1960

valid_size = 20

test_size = 400

valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(train_datasets, train_size, valid_size)
_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)

print('Training:', train_dataset.shape, train_labels.shape)
print('Validation:', valid_dataset.shape, valid_labels.shape)
print('Testing:', test_dataset.shape, test_labels.shape)


def randomize(dataset, labels):
	 permutation = np.random.permutation(labels.shape[0])
	 shuffle_dataset = dataset[permutation, :, :]
	 shuffle_labels = labels[permutation]
	 return shuffle_dataset, shuffle_labels


train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)

# prop 4
plt.title(train_labels[0])
plt.imshow(train_dataset[0], cmap="Greys_r")
plt.show()

pickle_file = os.path.join(data_root, 'data.pickle')
if not os.path.exists(pickle_file):
    try:
      f = open(pickle_file, 'wb')
      save = {
          'train_dataset': train_dataset,
          'train_labels': train_labels,
          'valid_dataset': valid_dataset,
          'valid_labels': valid_labels,
          'test_dataset': test_dataset,
          'test_labels': test_labels
      }
      pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
      f.close()
    except Exception as e:
      print("File is not saved")
      raise

statinfo = os.stat(pickle_file)
print("File size: ", statinfo.st_size)

#Prediction

train_num = 1000
X_train = train_dataset.reshape(-1, image_size*image_size)
y_train = train_labels

logreg = LogisticRegression()
logreg.fit(X_train, y_train)

X_valid = valid_dataset.reshape(-1, image_size*image_size)
y_valid = valid_labels

X_test = test_dataset.reshape(-1,image_size*image_size)
y_test = test_labels

valid_acc = sum(logreg.predict(X_valid) == y_valid)/len(y_valid)
print("accuracy rate: %f" % valid_acc)

test_acc = sum(logreg.predict(X_test) == y_test)/len(y_test)
print("accuracy rate: %f" % test_acc)
