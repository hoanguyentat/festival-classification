# IMAGE CLASSIFICATION
## Phát biểu bài toán:
Service phân loại ảnh từ url
* Đầu vào: URL của một ảnh
* Đầu ra: phân loại xem ảnh đó có phải là festival hay không.

## Yêu cầu
* Python 3
* Falsk
* Flask_cors
* Tensorflow

## Cài đặt
Từ thư mục gốc trên terminal run:    ``` python server.py ```
API: localhost:5000/api/list
